'use strict';

module.exports = {

  trackerHandlers: {}, // observers

  init: function(trackers) {
    if (trackers.cooladata) {
      var cooladata = require('./trackers/cooladata');
      cooladata.init(this, trackers.cooladata.key);

      this.subscribe('cooladata', cooladata.handleEvent.bind(cooladata));
    }

    if (trackers.newrelic) {
      var newrelic = require('./trackers/newrelic');
      newrelic.init(this, trackers.newrelic.applicationId);
    }
  },

  setUserId: function(userId) {
    for (var trackerName in this.trackerHandlers) {
      var tracker = require('./trackers/' + trackerName);
      tracker.setUserId(userId);
    }
  },

  // subscribe observers to events received by this tracker
  subscribe: function(tracker, fn) {
    if (fn) {
      this.trackerHandlers[tracker] = fn;
    }
  },

  // send event to all observers
  emitEvent: function(action, trackersData) {
    for(var tracker in trackersData) {
      if (this.trackerHandlers[tracker]) {
        this.trackerHandlers[tracker].call(this, trackersData[tracker]);
      }
    }
  },

  injectIntoHead: function(code, description) {
    if (typeof window !== 'undefined') {
      var children = this.getInjectableCode(code, description);

      for (var i = 0; i < children.length; i++) {
        document.getElementsByTagName('head')[0].appendChild(children[i]);
      }
    }
  },

  getInjectableCode: function(code, description) {
    var startDescription = document.createComment('Start: ' + description);
    var endDescription = document.createComment('End: ' + description);

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.text = code;

    return [startDescription, script, endDescription];
  }
}