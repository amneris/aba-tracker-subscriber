'use strict';

module.exports = {

  name: 'Cooladata Event Tracking',
  trackerSubscriber: undefined,
  key: undefined,
  userId: undefined,

  init: function(trackerSubscriber, key) {
    this.trackerSubscriber = trackerSubscriber;
    this.key = key;

    this.injectIntoHead();
  },

  setUserId: function(userId) {
    this.userId = userId;
  },

  getInjectableCode: function() {
    return '(function(d,a){if(!a.__SV){var b,c,g,e;window.cooladata=a;a._i=[];a.init=function(b,c,f){function d(a,b){var c=b.split(".");2==c.length&&(a=a[c[0]],b=c[1]);a[b]=function(){a.push([b].concat(Array.prototype.slice.call(arguments,0)))}}var h=a;"undefined"!==typeof f?h=a[f]=[]:f="cooladata";g=["trackEvent","trackEventLater","trackPageload","flush","setConfig"];for(e=0;e<g.length;e++)d(h,g[e]);a._i.push([b,c,f])};a.__SV=1.2;b=d.createElement("script");b.type="text/javascript";b.async=!0;b.src="//cdn.cooladata.com/tracking/cooladata-2.1.12.min.js";c=d.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.cooladata||[]);'
      + 'cooladata.init({'
      + '"app_key": "' + this.key + '",'
      + '"track_pageload": false,'
      + '"img_src_get_request": true'
      + '});';
  },

  injectIntoHead: function() {
    this.trackerSubscriber.injectIntoHead(this.getInjectableCode(), this.name);
  },

  trackEvent: function(eventName, eventProperties) {
    if (typeof window !== 'undefined' && window.cooladata) {
      window.cooladata.trackEvent(eventName, eventProperties);
      console.groupCollapsed('Sent event to Cooladata -> ' + eventName);
      for(var property in eventProperties) {
        console.debug(property + ' -> ' + eventProperties[property]);
      }
      console.groupEnd();
    }
  },

  handleEvent: function(eventData) {
    if (eventData && eventData.eventName) {
      var eventName = eventData.eventName;
      var eventProperties = eventData.properties ? eventData.properties : {};

      eventProperties['event_timestamp_epoch'] = new Date().getTime();
      eventProperties['idSite'] = 1;

      if (this.userId) {
        eventProperties['user_id'] = this.userId;
      }

      this.trackEvent(eventName, eventProperties);
    }
  }
}